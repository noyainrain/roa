# --- !Ups

CREATE TABLE state (id TEXT PRIMARY KEY, date DATE NOT NULL);
CREATE TABLE maker (id TEXT PRIMARY KEY, typ INT, eta INT NOT NULL);
CREATE TABLE mine (id TEXT PRIMARY KEY, aluminium INT NOT NULL);
CREATE TABLE build_task (id TEXT PRIMARY KEY, typ INT NOT NULL);

INSERT INTO state VALUES ('state', 0);

# --- !Downs

DROP TABLE state;
DROP TABLE maker;
DROP TABLE mine;
DROP TABLE build_task;
