package roa

import collection.mutable.Stack
import java.time.LocalDate
import org.scalatest._

abstract class RoaSpec extends FlatSpec with Matchers

class GameSpec extends RoaSpec with BeforeAndAfter with Inside {
  var sim = new Simulation()

  before {
    sim = new Simulation()
  }

  "Simulation" should "enqueue a task on build" in {
    val task = sim.build(RobotTypes.Mine)
    sim.tasks should contain (task)
  }

  // TODO: move to own file
  "State" should "tick" in {
    sim.tick()
    sim.state.date should be (LocalDate.of(0, 1, 2))
  }

  it should "update mine on tick" in {
    sim.tick()
    sim.state.robots(1) match { case Mine(aluminium) => aluminium should be (10) }
  }
}
