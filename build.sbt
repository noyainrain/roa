import Dependencies._

lazy val root = (project in file(".")).
  enablePlugins(PlayScala).
  settings(
    inThisBuild(List(
      organization := "com.example",
      scalaVersion := "2.11.8",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "Hello",
    libraryDependencies ++= Seq(
      ws,
      scalaTest % Test,
      "com.typesafe.play" %% "play-slick" % "2.0.2",
      "com.typesafe.play" %% "play-slick-evolutions" % "2.0.2",
      "org.xerial" % "sqlite-jdbc" % "3.16.1"),
    unmanagedResourceDirectories in Assets += baseDirectory.value / "client"
  )
