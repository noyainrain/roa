package controllers

import javax.inject.{ Inject, Singleton }

import play.api.libs.concurrent.Execution.Implicits.defaultContext

import roa.Simulation

import scala.concurrent.duration.DurationInt

import play.api.Logger

import akka.actor.ActorSystem

import play.api.libs.ws.WSClient

import play.api.libs.json.Json

@Singleton
class Cron @Inject() (ws: WSClient, system: ActorSystem, sim: Simulation) {
  Logger.info("STARTED CRON")
  system.scheduler.schedule(0.seconds, 10.seconds) {
    Logger.info("TICK")
    sim.tick()
    /*val data = Json.obj("text" -> sim.state.toString)
    //val data = Json.obj()
    // TODO move to config
    val url = "https://hooks.slack.com/services/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"
    ws.url(url).post(data).map { response =>
      if (response.status != 200) {
        Logger.info(s"SLACK ERROR ${response.status} ${response.allHeaders} ${response.body}")
      }
    } recover {
      case e => Logger.info("OH NOES ERROR " + e.toString)
    }*/
  }
}
