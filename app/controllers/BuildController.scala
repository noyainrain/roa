package controllers

import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json._
import play.api.mvc._
import play.api.mvc.BodyParsers.parse

import roa.{ RobotTypes, Simulation }

import javax.inject.Inject

class BuildController @Inject() (sim: Simulation) extends Controller {
  def o() = Action { _ => Ok("").withHeaders(
    "Access-Control-Allow-Origin" -> "*",
    "Access-Control-Allow-Headers" -> "Content-Type") }

  def post() = Action(parse.urlFormEncoded) { request =>
    // TODO load token from config and verify $token
    // NOTE later use $user_name and $user_id for identification
    // TODO route with $command and $text
    // TODO implement help
    println(request)
    println(request.body)
    val typ = request.body("text")(0).toLowerCase match {
      case "maker" => Some(RobotTypes.Maker)
      case "mine" => Some(RobotTypes.Mine)
      case other => None
    }
    typ match {
      case Some(t) => {
        sim.build(t)
        Ok(Json.obj("response_type" -> "in_channel", "text" -> "Enqueued a build task")).withHeaders("Access-Control-Allow-Origin" -> "*")
      }
      case None => Ok(Json.obj("text" -> "Sorry, what kind of robot, Maker or Mine?")).withHeaders("Access-Control-Allow-Origin" -> "*")
    }
  }

  def state() = Action.async { request =>
    for (state <- sim.state)
      yield Ok(Json.toJson(state)).withHeaders("Access-Control-Allow-Origin" -> "*")
  }
}
