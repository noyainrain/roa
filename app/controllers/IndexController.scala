package controllers

import javax.inject.Inject

import play.api.mvc._

import roa.Simulation
import roa.RobotTypes

import play.api.libs.concurrent.Execution.Implicits.defaultContext

class IndexController @Inject() (cron: Cron, sim: Simulation) extends Controller {
  def index(id: String = "lol", num: Int = 3) = Action.async { request =>
    //Ok(views.html.index())
    //Ok(s"hello world $request yeah ($id, $num) " + routes.IndexController.index("x", 42))
    for {
      _ <- sim.build(RobotTypes.Mine)
      tasks <- sim.getTasks
    } yield Ok(s"${sim.state.toString}\n${tasks}")
  }
}
