import scala.util.Random

/** A sample game package.
 *
 *  {{{
 *  scala> foo
 *  bar
 *  }}}
 */
package object roa {
  def randomString(length: Int = 8): String = Random.alphanumeric.take(length).mkString
}
