package roa

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Future
import java.time.LocalDate
import java.sql.Date
import javax.inject.{ Inject, Singleton }

import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.{ Json, Writes }
import slick.driver.JdbcProfile
import slick.driver.SQLiteDriver.api._

object RobotTypes extends Enumeration {
  val Maker, Mine = Value
}

/*object Game extends App {
  private val sim = new Simulation()

  sim.tick()
  sim.tick()
  sim.build(RobotTypes.Mine)
  sim.build(RobotTypes.Mine)
  sim.build(RobotTypes.Maker)
  sim.tick()
  sim.tick()
  sim.tick()
  sim.tick()
  sim.tick()
  sim.tick()
  sim.tick()
  sim.tick()
  sim.build(RobotTypes.Mine)
  sim.tick()
  sim.tick()
  sim.tick()
  sim.tick()
  sim.tick()
  sim.tick()
}*/


// TODO: Do database foo only in Simulation (only api methods are build and tick and delete task
// anyhow)
/** Simulates something.
 *
 *  How nice is this? This uses [[scala.Option]].
 *
 *  @constructor does create.
 *  @param game the game
 */
@Singleton // TODO: remove once on db
class Simulation @Inject() (dbConfigProvider: DatabaseConfigProvider) {
  private val dbConfig = dbConfigProvider.get[JdbcProfile]
  private val db = dbConfig.db

  private val states = TableQuery[StateTable]

  def state: Future[State] = db.run(states.result.headOption).map { s => s.get }

  //var tasks = ArrayBuffer[Task]()
  //var state = State(LocalDate.of(0, 1, 1), Vector(Maker(), Mine()), Vector())

  def update(): Future[Unit] = {
    /*
    //import dbConfig.profile.api._
    import slick.driver.SQLiteDriver.api._
    //import slick.driver.SQLiteDriver.api._
    //val mines = TableQuery[Mne]
    db.run(tasks.schema.create)*/
    Future.successful()
  }

  val makers = TableQuery[MakerTable]
  val mines = TableQuery[MineTable]

  /** Performs a tick.
   *
   *  @param a something
   *  @return other thing
   */
  def tick(): Future[Unit] = {
    //var state = State(LocalDate.of(0, 1, 1), Vector(Maker(), Mine()), Vector())
    /*val result = state.tick(tasks)
    state = result._1
    tasks.clear()
    tasks ++= result._2
    println(s"tick:  $state")*/
    //val ts = tasks.toVector
    //val (rs, openTasks) = startBuild(runBuild(for (r <- robots) yield r.tick), ts)
    /*for {
      states <- db.run(states.map(s => s).result)
    } yield states.head.tick(db)
    val states = TableQuery[StateTable]
    for {
      _ <- db.run(states.filter(s => s.id === id).map(s => s.date).update(Date.valueOf(date.plusDays(1))))
      s <- db.run(states.filter(s => s.id === id).result)
    } yield s(0)*/

    for {
      state <- this.state
      _ <- tickRobots()
      _ <- startTasks()
      _ <- db.run(states.filter(s => s.id === "state").map(s => s.date).update(Date.valueOf(state.date.plusDays(1))))
    } yield 0
  }

  private def tickRobots(): Future[Unit] = {
    for {
      robots <- this.robots
      _ <- for (r <- robots) yield r match {
        case Maker(id, Some(typ), eta) => {
          if (eta == 1) {
            db.run(DBIO.seq(
              makers.filter(m => m.id === id).map(m => (m.typ.?, m.eta)).update(None, 0),
              typ match {
                case RobotTypes.Maker => makers ++= Maker()
                case RobotTypes.Mine => mines ++= Mine()
              }))
          } else {
            db.run(makers.filter(m => m.id === id).map(m => m.eta).update(eta - 1))
          }
        }
        case Mine(id, aluminium) => {
          val aluminium = aluminium + (Output + AluminiumAbundance).toInt
          db.run(mines.filter(m => m.id === id).map(m => m.aluminium).update(aluminium))
        }
      }
    } yield 0
  }

  private def startTasks(): Future[Unit] = {
    for {
      tasks <- getTasks
      _ <- for (task <- tasks) yield task match {
        case BuildTask(_, typ) => {
          for {
            maker <- makers.filter(m => !m.typ.isDefined)
            _ <- makers.headOption match {
              case Some(Maker(id, _, _)) => db.run(DB.seq(
                makers.filter(m => m.id === id).map { m => (m.typ, m.eta) } update(Some(task.typ), 5),
                tasks.filter(t => t.id === task.id).delete))
            }
          } yield 0
        }
      }
    } yield 0
  }

  def build(typ: RobotTypes.Value): Future[BuildTask] = {
    val task = BuildTask(randomString(), typ)
    val tasks = TableQuery[BuildTaskTable]
    val mines = TableQuery[MineTable]
    // better with DBIO.seq
    for {
      _ <- db.run(mines += Mine(randomString(), 42))
      _ <- db.run(tasks += task).map(_ => task)
    } yield task
    //task
  }

  def robots: Future[Seq[Robot]] = {
    return db.run(DBIO.sequence(Seq(makers.result, mines.result))) map { r => r(0) ++ r(1) }
    /*return for {
      makers <- db.run(makers.result)
      mines <- db.run(mines.result)
    } yield makers ++ mines*/
  }

  def getTasks: Future[Seq[BuildTask]] = {
    val tasks = TableQuery[BuildTaskTable]
    db.run(tasks.result).map(t => t)
  }

  //println(s"start: $state")
}

//case class State(val id: String, val date: LocalDate, val robots: Seq[Robot], val tasks: Seq[Task]) {
case class State(val id: String, val date: LocalDate) {
  /*def tick(tasks: Seq[Task]): (State, Seq[Task]) = {
    val ts = tasks.toVector
    val (rs, openTasks) = startBuild(runBuild(for (r <- robots) yield r.tick), ts)
    (State(date.plusDays(1), rs, ts), openTasks)
  }

  private def startBuild(robots: Seq[Robot], tasks: Seq[Task]): (Seq[Robot], Seq[Task]) = {
    if (robots.isEmpty || tasks.isEmpty) return (robots, tasks)
    (robots.head, tasks.head) match {
      case (Maker(id, None, _), BuildTask(_, typ)) => {
        val (rs, ts) = startBuild(robots.tail, tasks.tail)
        (Maker(id, Some(typ), 5) +: rs, ts)
      }
      case other => {
        val (rs, ts) = startBuild(robots.tail, tasks)
        (robots.head +: rs, ts)
      }
    }
  }

  private def runBuild(robots: Seq[Robot]): Seq[Robot] = {
    if (robots.isEmpty) return robots
    robots.head match {
      case Maker(id, typ, eta) if eta > 1 => Maker(id, typ, eta - 1) +: runBuild(robots.tail)
      case Maker(id, Some(typ), eta) if eta == 1 => Maker(id) +: runBuild(robots.tail) :+ (typ match {
        case RobotTypes.Maker => Maker()
        case RobotTypes.Mine => Mine()
      })
      case other => robots.head +: runBuild(robots.tail)
    }
  }*/
}

object State {
  implicit val stateWrites = Json.writes[State]
}

trait Robot {
  def tick(): Robot = this
}

object Robot extends Writes[Robot] {
  implicit val robotWrites = Robot
  implicit val mineWrites = Json.writes[Mine]
  implicit val makerWrites = Json.writes[Maker]

  def writes(robot: Robot) =
    robot match {
      case r: Mine => Json.toJson(r)(mineWrites)
      case r: Maker => Json.toJson(r)(makerWrites)
    }
}

case class Maker(val id: String = randomString(), val typ: Option[RobotTypes.Value] = None, val eta: Int = 0) extends Robot

case class Mine(val id: String = randomString(), val aluminium: Int = 0) extends Robot {
  val Output = 1000
  val AluminiumAbundance = 0.01

  override def tick() = Mine(id, aluminium + (Output * AluminiumAbundance).toInt)
}

trait Task

object Task extends Writes[Task] {
  implicit val taskWrites = Task
  implicit val buildTaskWrites = Json.writes[BuildTask]

  def writes(task: Task) =
    task match {
      case t: BuildTask => Json.toJson(t)(buildTaskWrites)
    }
}

case class BuildTask(val id: String = randomString(), val typ: RobotTypes.Value) extends Task

trait ObjectColumns[T] extends Table[T] {
  def id = column[String]("id")
}

class StateTable(val tag: Tag) extends Table[State](tag, "state") with ObjectColumns[State] {
  def date = column[Date]("date")
  def * = (id, date) <> (
    (t: (String, Date)) => State(t._1, t._2.toLocalDate),
    (t: State) => Some(t.id, Date.valueOf(t.date)))
}

class MakerTable(val tag: Tag) extends Table[Maker](tag, "maker") with ObjectColumns[Maker] {
  def typ = column[Int]("typ")
  def eta = column[Int]("eta")
  def * =
    (id, typ.?, eta) <> (
      (t: (String, Option[Int], Int)) => Maker(t._1, t._2 match { case Some(f) => Some(RobotTypes(f)); case other => None }, t._3),
      (t: Maker) => Some((t.id, t.typ match { case Some(f) => Some(f.id); case other => None }, t.eta)))
}

class MineTable(val tag: Tag) extends Table[Mine](tag, "mine") with ObjectColumns[Mine] {
  def aluminium = column[Int]("aluminium")
  def * = (id, aluminium) <> (Mine.tupled, Mine.unapply)
}

class BuildTaskTable(val tag: Tag) extends Table[BuildTask](tag, "build_task") with ObjectColumns[BuildTask] {
  def typ = column[Int]("typ")
  def * =
    (id, typ) <> (
      (t: (String, Int)) => BuildTask(t._1, RobotTypes(t._2)),
      (t: BuildTask) => Some((t.id, t.typ.id)))
}
