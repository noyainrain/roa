package example

class Sim {
}

object Game extends App {
  implicit val sim = new Sim()

  println("MAIN " + args)
  //println(greeting)
  var x = 3
  println("Hi")
  println(1 + 1)
  x = x + 3
  println(x + 3)

  val f = (test: String) => test + " jawohl"
  println("f " + f("du"))

  def e(a: Int): Int = 42 - a
  println("e " + e(3))

  println(Robot.create("hello").id)
  val robot = new Robot("abc")
  robot.build("mine")

  val p = Point(1, 2)
  println(p)
  val p2 = p.copy(y = 8)
  println(p2)

  val mine = new Mine("Super mine")
  mine.install()
  mine.destroy()
  mine.doTyped(3.23)

  // return type is not needed
  def plusX(x: Int): Int => Int = (a: Int) => a + x
  val plus4 = plusX(4)
  println(plus4(3) + " == 7")

  def plusY(y: Int)(a: Int) = a + y
  // TODO: return type needed here because of function convertion, why?
  val plus5: Int => Int = plusY(5)
  println(plus5(3) + " == 8")

  // TODO: not possible?
  //val plusX(x: Int) => (a: Int) => a + x

  //val type = (Int, String) => Unit
  //println("type " + type)

  val p3 = Point3D(4, 5, 6)
  def matcher(v: Any) = {
    v match {
      case Point(_, y) if y == 8 => println("match point with y being 8")
      case Point(_, _) => println("match point")
      case Point3D(_, _, _) => println("match point3d")
      case 42 => println("the meaning of life")
      case Seq(a, b) => println("seq2 " + a + ", " + b)
      case Seq(a, b, rest @ _*) => println(s"seq $a $b")
      case Times3(x) => println("times 3 -> " + x)
      case other => println("match none")
    }
  }
  matcher(p)
  matcher(p2)
  matcher(p3)
  matcher(42)
  matcher(43)
  var seq: Seq[Char] = "quantensprung"
  matcher(seq)
  seq = "wb"
  matcher(seq)
  matcher(8)
  matcher(9)

  //val html = <html><body>nice</body></html>
  //println(html)

  println(0 until 30 toList)
  println(for (i <- 0 until 30 if i % 3 == 0) yield i)
  println(for (i <- 0 until 30 if i % 3 == 0) yield i + 1)
  println(for (y <- 0 until 5; x <- 0 until 5) yield (x, y))
  for (i <- 0 until 3) println("hello 3x")

  val part = new Robot.Part("Foot")
  println(part.name)

  // TODO abgefahren mit type self und co :p
  def foobar(a: String) = {
    type X = Int
    32
  }

  def something(x: Int) {
    println(s"something $x")
  }
  something { 12 }

  def context(body: => Unit) {
    println("pre")
    body
    println("post")
  }

  context {
    println("im in a context")
  }

  println(Point())

  robot.id = "HEELLOO"
}

object Times3 {
    def unapply(x: Int) = if (x % 3 == 0) Some(x / 3) else None
}

class Robot(private var _id: String) {
  def build(xid: String): Unit = println("building " + xid + "(" + _id + ")")

  def id = _id

  def id_=(value: String) {
      println("setter")
      _id = value
  }
}

object Robot {
    class Part(val name: String)

    def create(id: String): Robot = new Robot(id)
}

class Mine(id: String)(implicit val sim: Sim) extends Robot(id) with Building {
    override def install(): Unit = println("installing FASTER ")

    def destroy(): Unit = println("destorying " + id + "...")

    def doTyped[T](x: T) = println("type is free " + x)
}

// TODO: cannot inherit from case class, woot
class MathValue

case class Point(x: Int = 0, y: Int = 0)

case class Point3D(x: Int, y: Int, z: Int)

trait Building {
    lazy val greeting: String = "hello"

    def install(): Unit = println("installing building ")

    def destroy(): Unit
}
