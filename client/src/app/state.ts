export class State {
    date: string;
    cmd: string;
    robots: Robot[];
}

export class Robot {
    id: string;
}
