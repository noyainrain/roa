import { Injectable } from '@angular/core';

import { State, Robot } from './state';

//const ROBOTS: Robot[] = [{id: "foo"}, {id: "bar"}, {id: "oink"}];
//const STATE: State = { date: '2017-01-02', robots: ROBOTS, tasks: [] }

@Injectable()
export class StateService {
    getState(): Promise<State> {
        //return Promise.resolve(new State());
        return fetch('//localhost:9000/api/state').then(r => r.json()).then(s => {console.log(s); return s;});
    }

    send(msg: String): Promise<string> {
        let data = new URLSearchParams()
        let tokens = msg.split(' ');
        let cmd = tokens[0];
        let text = tokens.slice(1).join(' ');
        data.append('cmd', cmd);
        data.append('text', text);
        return fetch('//localhost:9000/api/cmd', {method: 'POST', body: data}).then(r => r.json()).then(r => r.text)
        //return Promise.resolve("foo");
    }
}
