import { Component, OnInit } from '@angular/core';
import { StateService } from './state.service';
import { State, Robot } from './state';

@Component({
    selector: 'roa-monitor',
    templateUrl: '/static/src/app/monitor.component.html',
    styles: [`
      ul { color: red; }
      .withcmd { color: blue; }
    `],
})
export class MonitorComponent implements OnInit {
    state = new State();

    constructor(private stateService: StateService) {}

    ngOnInit(): void {
        console.log("INIT");
        this.stateService.getState().then(state => this.state = state);
        //this.state = this.stateService.getState();
    }

    onClick(robot: Robot): void {
      alert('robot ' + robot.id);
    }
}
