import { Component, Input, OnInit } from '@angular/core';
import { StateService } from './state.service';
import { State } from './state';

@Component({
    selector: 'chat',
    template: `
        <div>
            <p>Chat:</p>
            <!--<h2 [class.withcmd]="state.cmd">Hello World!</h2>-->
            <!--<form>-->
            <input [(ngModel)]="msg" placeholder="Message">
            <button (click)="send()">Send</button>
            <!--</form>-->
            <p>{{reply}}</p>
            <!-- <p *ngIf="msg">Current cmd: {{state.cmd}}</p> -->
        </div>
    `
})
export class ChatComponent {
    msg = "";
    reply = "";

    constructor(private stateService: StateService) {}

    /*ngOnInit(): void {
        this.state = this.stateService.getState();
    }*/

    send(): void {
        this.stateService.send(this.msg).then(reply => this.reply = reply);
    }

    /*@Input()*/ state: State;
}
