namespace roa {

export class Robot {
    fullName: string;

    constructor(public firstName: string, public middleInitial: string, public lastName: string) {
        this.fullName = firstName + " " + middleInitial + " " + lastName;
    }
}

interface Person {
    firstName: string;
    lastName: string;
}

export function greeter(person: Robot) {
    return "Hello, " + person.fullName;
}

}
