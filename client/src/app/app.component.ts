import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
      <h1>{{title}}</h1>
      <a routerLink="/chat" routerLinkActive="act">Chat</a>
      <a routerLink="/monitor" routerLinkActive="act">Monitor</a>
      <router-outlet></router-outlet>
  `,
  styles: [`a.act { color: green; font-size: 2em; }`]
})
export class AppComponent {
    title = 'Robots on Asteroids';
}
