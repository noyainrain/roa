import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AppComponent }  from './app.component';
import { MonitorComponent } from './monitor.component';
import { ChatComponent } from './chat.component';
import { StateService } from './state.service';

@NgModule({
  imports: [BrowserModule, FormsModule, RouterModule.forRoot([
    {
        path: '',
        redirectTo: '/chat',
        pathMatch: 'full'
    },
    {
        path: 'chat',
        component: ChatComponent
    },
    {
        path: 'monitor',
        component: MonitorComponent
    }
  ])],

  declarations: [ AppComponent, MonitorComponent, ChatComponent ],
  bootstrap:    [ AppComponent ],
  providers: [StateService]
})
export class AppModule { }
